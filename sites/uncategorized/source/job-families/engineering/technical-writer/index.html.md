---
layout: job_family_page
title: "Technical Writer"
---

At GitLab, our [team of technical writers](https://about.gitlab.com/handbook/engineering/ux/technical-writing/) is responsible for ensuring that the [documentation](https://docs.gitlab.com/) for all of our products is clear, correct, comprehensive, and easy to use. We are looking for great writers with strong technical proficiencies to help our users succeed with our [rapidly evolving suite of DevOps tools](https://about.gitlab.com/releases/).

As a technical writer and as part of our growing team, you’ll:

- Collaborate with GitLab engineers, who typically write the first draft of documentation for
  the new features they create.
- Dive in on special projects, planning and authoring new content, and helping to develop new
  docs site features and processes.
- Collaborate with others across the organization to craft new and improved content. You’ll be
  at the leading edge of DevOps while contributing to one of the [world’s largest open-source projects](https://about.gitlab.com/blog/2017/07/06/gitlab-top-30-highest-velocity-open-source/)
  and engaging with our wider community.

The [General requirements](#general-requirements) are core skills and expectations common to all the technical writing job grades. The [Responsibilities](#responsibilities) describe the expectations of the work we do, and how it varies in scope and complexity across the job grades.

# General requirements

- You have:
   - Experience writing, editing, researching, and planning software documentation.
   - Excellent skills in grammar, minimalist documentation design, and effective information architecture.
   - Great teaching skills that translate into amazing written work.
   - Experience using the Linux shell, command-line Git, HTML/CSS, and/or at least one programming language (does not have to be in a professional context).
   - Experience using static site generators and managing docs as code.
   - Experience with some of the following:
      - Using or documenting DevOps tools.
      - JavaScript and front-end development.
      - Advanced programming or other technical experience.
      - Using GitLab.
      - A rapidly scaling start-up environment.
      - Remote work; especially in collaboration with others across countries and time zones.
- You are:
   - Highly organized; able to triage and prioritize numerous issues and projects.
   - Able to succeed in a remote, globally distributed work environment.
- You share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values.
- You have experience with workflows and tooling used by engineering, operations, and product teams.

## Responsibilities

The responsibilities for technical writers at GitLab are based on these levels:

- [Technical Writer](#technical-writer-responsibilities)
- [Senior Technical Writer](#senior-technical-writer-responsibilities)
- [Staff Technical Writer](#staff-technical-writer-responsibilities)

### Technical Writer responsibilities

The Technical Writer is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

- **Content**: Create, merge, and maintain product documentation and release posts for assigned product groups. Balance priorities.
- **Structure**: Ensure newly developed content uses [content topic types](https://docs.gitlab.com/ee/development/documentation/structure.html) to make product documentation easier to scan.
- **Organization**: Ensure that product documentation pages in your assigned groups are well organized by using good naming and logical groupings.
- **User interface**: Create or improve user interface text, such as field labels or error messages, with a focus on your assigned groups.
- **Reviews**: Provide effective reviews for content created by others mainly within your assigned groups, based on relevant style guides.
- **Product knowledge**: Understand the technology and features of the development groups to which you’re assigned.
- **Process**: Contribute [Handbook](/handbook/) changes that help the organization evolve the culture and best practices.
- **Communication**: Communicate effectively and professionally with other team members.
- **Leadership**: Act as the Technical Writing lead for assigned groups.

### Senior Technical Writer responsibilities

The Senior Technical Writer is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

- **Content**: Efficiently create, merge, and maintain product documentation and release posts for assigned product groups. Guide improvements to merge requests. Suggest and balance priorities.
- **Structure**: Implement content topic types in both new and previously created content to make product documentation easier to scan.
- **Organization**: Ensure that product documentation pages are well organized in your assigned stage by using good naming and logical groupings.
- **User interface**: Actively create or improve user interface text, such as field labels or error messages, with a focus on your assigned stage.
- **Reviews**: Provide effective and efficient reviews for content created by others across your assigned stage, based on relevant style guides.
- **Product knowledge**: Deeply understand the technology and features of the development groups to which you’re assigned, and have a general understanding of your assigned stage.
- **Process**: Contribute [Handbook](/handbook/) changes that help the organization evolve the culture and best practices.
- **Communication**: Communicate effectively and professionally with other team members.
- **Leadership**: Act as the Technical Writing lead for assigned groups and stages.
- **Recruiting**: Be available to interview potential technical writing candidates.
- **Mentoring**: Mentor other technical writers and team members regarding best practices and other process knowledge.

### Staff Technical Writer responsibilities

The Staff Technical Writer is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

- **Content**: Efficiently create and maintain product documentation and release posts for assigned product groups or stages. Guide improvements to merge requests.
- **Structure**: Actively implement content topic types in both new and previously created content to make product documentation easier to scan.
- **Organization**: Ensure that product documentation pages are well organized across stages by using good naming and logical groupings.
- **User interface**: Create model examples of user interface text, including field labels or error messages, with a cross-stage focus. Actively improve style guide and [Pajamas](/handbook/engineering/ux/pajamas-design-system/) content related to user interface text.
- **Reviews**: Model review practices that improve the quality of contributions over time. Support and guide team members to improve reviews in their groups and stages.
- **Product knowledge**: Understand the technology and features of both the development stage to which you’re assigned and the end-to-end GitLab product to help improve the information architecture of the docs site.
- **Process**: Actively contribute [Handbook](/handbook/) changes that help the organization evolve the culture and best practices.
- **Communication**: Communicate effectively and professionally with other team members, and be a model for collaboration.
- **Leadership**: Identify and lead cross-stage projects that improve the deliverables or processes of the Technical Writing team.
- **Recruiting**: Be available to interview potential technical writing candidates.
- **Mentoring**: Mentor other technical writers and team members about suggesting and balancing priorities, industry best practices, and other process knowledge.



## Career Ladder

For more details on the engineering career ladders, review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.

## Performance indicators

- [Technical Writing team member MR rate](/handbook/engineering/ux/performance-indicators/#technical-writing-team-member-mr-rate)
- [Distribution of Technical Writing team documentation effort](/handbook/engineering/ux/performance-indicators/#distribution-of-technical-writing-team-documentation-effort)

## Hiring Process

Candidates for this position can expect the hiring process to follow the process below. Please keep in mind that you can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their name or job title on our [team page](https://about.gitlab.com/company/team/).

1. Candidates who demonstrate an interest in the role will be asked to share work samples and respond to questions related to technical writing.
1. Selected candidates will be invited to schedule a 30-minute [screening call](/handbook/hiring/interviewing/#conducting-a-screening-call) with one of our Global Recruiters. In this call, we will discuss your experience, understand what you are looking for in a Technical Writing role, talk about your work and approach to technical writing, discuss your compensation expectations and reasons why you want to join GitLab, and answer any questions you have.
1. If you successfully pass the screening call, you will be invited to schedule a 45-minute first interview with our Technical Writing Manager. In this interview, we will want you to talk through the approach you took in the examples of work you shared as part of your application, your experience up to now, understand what you're looking for in a technical writing position, and answer any questions you have.
1. The next interview is with one of our Technical Writers. We’ll discuss your experience, why you’re looking to join GitLab, your domain experience, and what it's like to be a technical writer at GitLab. We’ll assess your alignment with our [values](/handbook/values/#credit), and answer any questions you have.
1. Next, you’ll meet one of our Product Managers for a 45-minute interview. We’ll discuss your experience, why you’re looking to join GitLab, your domain experience, and what it's like to be a technical writer at GitLab. We’ll assess your alignment with our [values](/handbook/values/#credit), and answer any questions you have.
1. Finally, you’ll meet our Senior Manager, Technical Writing. At this stage, we’ll look to understand your views on how documentation impacts user experience, your experience up to now, you'll have a discussion around technical writing maxims, and the interviewer will answer any questions you have.
